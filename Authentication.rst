##############
Authentication
##############
This part of the API documentation is for anything related to authentication on the API.

------------------------
GET /api/RememberRequest
------------------------
This take no parameters.
Returns a JSON object:

::

	{
		AuthenticationResult=int
		CurrentUserForExpired=int
		DaysToPasswordExpire=int
		LoginSuccessful=bool
		PasswordExpired=bool
		Quiet=bool
	}

----------------
POST /api/SignIn
----------------
This takes no query parameters, but does have a request body:

::

	{
		From="" // only seen as a blank string
		InterfaceSource="WebApp" // only seen as WebApp, unknown what 
					 other parameters do/are.
		Password="password"
		Remember=False // At least on the instance I am testing on,
					 true/false does not make a difference.
		Username="username"
	}

Upon successful authentication, it will set a few cookies (session ID, functions as auth token) that you will have to send with any further requests.
Here is the response body:

::

	{
		AuthenticationResult=0 // 1 on unsuccessful login?
		CurrentUserForExpierd=int // Not sure what this does
		DaysToPasswordExpire=-1
		LoginSuccessful=True
		PasswordExpierd=False
		Quiet=False
		SignInActions={}
		Username="username"
	}
