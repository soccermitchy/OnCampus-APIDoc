.. OnCampus-APIDoc documentation master file, created by
   sphinx-quickstart on Fri Sep  2 22:02:21 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OnCampus-APIDoc's documentation!
===========================================

As a rule of thumb, all data sent/returned from their API is JSON.

Contents:

.. toctree::
   :maxdepth: 2


   Authentication

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

